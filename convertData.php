<?php
error_reporting(E_ALL);
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
set_time_limit(0);

//Setup
$inputdir = 'inputs/';
$outputdir = 'outputs/';

//Script Start
$files1 = scandir($inputdir);
$thetime = time();
$fp = fopen($outputdir . 'ConversionRun' . $thetime . '.csv', 'a');
fputcsv($fp, array('File Name','Row','UniProt Identifier','Protein','Amino Acid Letter','Modification Number','Modification Name','AScore','Position',
	'Peptide','Orignal Peptide String','-7','-6','-5','-4','-3','-2','-1','0','+1','+2','+3','+4','+5','+6','+7','+8','Area'));
fclose($fp);

foreach($files1 as $file) {
	if (explode('.',$file)[1] == 'csv') {
		echo 'Processing ' . $file;

		$rows = 1;
		$flag = true;

		if (($handle = fopen($inputdir . $file, "r")) !== FALSE) {
    		while (($data = fgetcsv($handle)) !== FALSE) {
    			if($flag) { $flag = false; continue; }
    			
    			$rows++;
 				
 				//$totalrows++;
 				$row = array();

    			//AScores by spot
    			$Ascores = explode(';',$data[19]);

    			//Clip Ends
                $pep16 = $data[3];

                if ($pep16[1] == '.') {
                    $pep16 = substr($pep16,2);
                }

                if ($pep16[strlen($pep16)-2] == '.') {
                    $pep16 = substr($pep16,0,-2);
                }

    			//Find Hits
    			$peparray = str_split($pep16);

    			//Clean Peptide String
    			$cleanpeptide = array();

    			for ($c=0;$c<sizeof($peparray);$c++) {
    				if (ctype_alpha($peparray[$c])) {
    					array_push($cleanpeptide, $peparray[$c]);
    				}
    			}

    			$hits = 0;
    			$offsets = 0;

				for ($p=0;$p<sizeof($peparray);$p++) {

    				if (($p+1) != sizeof($peparray) && $peparray[$p+1] == '(') {
                        
    					//File Name
    					array_push($row,$file);

    					//Row
    					array_push($row,$rows);

    					$proteinworking = $data[1];
    					if (substr($proteinworking,0,3) == 'tr|') {
    						$proteinworking = substr($proteinworking, 3, strlen($proteinworking));
    					}

    					$protiensplit = explode('|',$proteinworking);

    					//UniProt Identifier
    					array_push($row,$protiensplit[0]);

    					//Protein
    					array_push($row,$protiensplit[1]);

    					//Amino Acid Letter
    					array_push($row,$peparray[$p]);

    					$nextparen = strpos($pep16,')',$p);
    					$hittype = substr($pep16,$p+1,$nextparen-($p));

    					//Modification Number
    					array_push($row,$hittype);

    					$ascoredetails = explode(':',$Ascores[$hits]);

    					//Modifcation Name
    					array_push($row,$ascoredetails[1]);

    					//AScore
    					array_push($row,$ascoredetails[2]);

    					//Position
    					array_push($row,($data[16]+$p-$offsets));

    					//Clean Peptide String
    					array_push($row, implode($cleanpeptide));

                        //Original Peptide String
                        array_push($row, $pep16);

    					
                        //Position -7
                        if ($p-$offsets-7 >=0) {
                            array_push($row, $cleanpeptide[$p-$offsets-7]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position -6
                        if ($p-$offsets-6 >=0) {
                            array_push($row, $cleanpeptide[$p-$offsets-6]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position -5
                        if ($p-$offsets-5 >=0) {
                            array_push($row, $cleanpeptide[$p-$offsets-5]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position -4
                        if ($p-$offsets-4 >=0) {
                            array_push($row, $cleanpeptide[$p-$offsets-4]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position -3
                        if ($p-$offsets-3 >=0) {
                            array_push($row, $cleanpeptide[$p-$offsets-3]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position -2
                        if ($p-$offsets-2 >=0) {
                            array_push($row, $cleanpeptide[$p-$offsets-2]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position -1
                        if ($p-$offsets-1 >=0) {
                            array_push($row, $cleanpeptide[$p-$offsets-1]);
                        } else {
                            array_push($row, '-');
                        }

                        //The Hit (0)
                        array_push($row, $cleanpeptide[$p-$offsets]);

                        //Position +1
                        if ($p-$offsets+1 <= (sizeof($cleanpeptide)-1)) {
                            array_push($row, $cleanpeptide[$p-$offsets+1]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position +2
                        if ($p-$offsets+2 <= (sizeof($cleanpeptide)-1)) {
                            array_push($row, $cleanpeptide[$p-$offsets+2]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position +3
                        if ($p-$offsets+3 <= (sizeof($cleanpeptide)-1)) {
                            array_push($row, $cleanpeptide[$p-$offsets+3]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position +4
                        if ($p-$offsets+4 <= (sizeof($cleanpeptide)-1)) {
                            array_push($row, $cleanpeptide[$p-$offsets+4]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position +5
                        if ($p-$offsets+5 <= (sizeof($cleanpeptide)-1)) {
                            array_push($row, $cleanpeptide[$p-$offsets+5]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position +6
                        if ($p-$offsets+5 <= (sizeof($cleanpeptide)-1)) {
                            array_push($row, $cleanpeptide[$p-$offsets+5]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position +7
                        if ($p-$offsets+6 <= (sizeof($cleanpeptide)-1)) {
                            array_push($row, $cleanpeptide[$p-$offsets+6]);
                        } else {
                            array_push($row, '-');
                        }

                        //Position +8
                        if ($p-$offsets+7 <= (sizeof($cleanpeptide)-1)) {
                            array_push($row, $cleanpeptide[$p-$offsets+7]);
                        } else {
                            array_push($row, '-');
                        }

                        //Area
                        array_push($row, $data[14]);
                        

    					$offsets = $offsets + strlen($hittype);
    					$p = $p + strlen($hittype);
    					$hits++;

    					//Write row to CSV Array
    					$fp = fopen($outputdir . 'ConversionRun' . $thetime . '.csv', 'a');
    					fputcsv($fp, $row);
    				    fclose($fp);
    					$row = array();
    					
					}
    			}
    		}

    		fclose($handle);
		}
		
		if (defined('STDIN')) {
			$prettyOutput = " Rows Processed \n";
		} else {
			$prettyOutput = " Rows Processed </br>";
		}

		echo ' - ' . $rows . $prettyOutput;
	}
}

?>